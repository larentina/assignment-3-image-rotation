#ifndef BMP_H
#define BMP_H

#include <stdint.h>
#include <stdio.h>

#include "image.h"
#define BMP_TYPE 0x4D42
#define BMP_OFFBITS 54
#define BMP_RESERVED 0
#define BMP_SIZE 40
#define BMP_PLANES 1
#define BMP_BITCOUNT 24
#define BMP_COMPRESSION 0
#define BMP_SIZEIMAGE 0
#define BMP_XPELSPERMETER 0
#define BMP_YPELSPERMETER 0
#define BMP_CLRUSED 0
#define BMP_CLRIMPORTANT 0
#define PADDING_BYTE 0
#define BMP_HEADER_SIZE (sizeof(struct bmp_header))
#define GET_IMAGE_SIZE(image) ((image)->width * (image)->height * sizeof(struct pixel))
#define GET_ROW_PADDING(image) ((4 - ((image)->width*3) % 4) % 4)
struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum read_status {
    READ_OK = 0,
    READ_FAILURE,
    READ_INVALID_HEADER
};

enum write_status {
    WRITE_OK = 0,
    WRITE_FAILURE
};
enum write_status to_bmp(FILE *output_file, struct image const *image);
enum read_status from_bmp(FILE *input_file, struct image *image);

#endif
