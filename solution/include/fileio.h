
#ifndef FILEIO_H
#define FILEIO_H

#include <stdio.h>

enum io_status{
    IO_SUCCESS =0 ,
    IO_FAILURE
};
enum io_status open_file(char* file_name, FILE** input_file,char* mode );
enum io_status close_file(FILE* output_file);
#endif
