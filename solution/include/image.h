
#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

struct pixel {
    uint8_t r, g, b;
};

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel *data;
};

struct image rotate(struct image const source);
struct image* rotate_to_angle(struct image source,int32_t angle);
void clear_image(struct image* image );
#endif

