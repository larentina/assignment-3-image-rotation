#include "fileio.h"
enum io_status open_file(char* file_name, FILE** input_file,char* mode ){
    *input_file = fopen(file_name, mode);
    if(*input_file){
        return IO_SUCCESS;
    }
    return IO_FAILURE;
}
enum io_status close_file(FILE* output_file){
    if(fclose(output_file))
    {
        return IO_FAILURE;
    }
    return IO_SUCCESS;
}
