
#include "bmp_file.h"
#include <stdio.h>
#include <stdlib.h>


static struct bmp_header* create_bmpheader(struct image const *image){
    struct bmp_header* header = malloc(BMP_HEADER_SIZE);

            header->bfType = BMP_TYPE;
            header->bfileSize = BMP_HEADER_SIZE + (GET_IMAGE_SIZE(image));
            header->bfReserved = BMP_RESERVED;
            header->bOffBits = BMP_OFFBITS;
            header->biSize = BMP_SIZE;
            header->biWidth = image->width;
            header->biHeight = image->height;
            header->biPlanes = BMP_PLANES;
            header->biBitCount = BMP_BITCOUNT;
            header->biCompression = BMP_COMPRESSION;
            header->biSizeImage = BMP_SIZEIMAGE;
            header->biXPelsPerMeter = BMP_XPELSPERMETER;
            header->biYPelsPerMeter = BMP_YPELSPERMETER;
            header->biClrUsed = BMP_CLRUSED;
            header->biClrImportant = BMP_CLRIMPORTANT  ;

    return header;
}
static void clear_bmpheader(struct bmp_header* header){
    free(header);
}



static void write_bmpheader(FILE *output_file, struct image const *img) {
    struct bmp_header* header = create_bmpheader(img);
    fwrite(header, BMP_HEADER_SIZE, 1, output_file);
    clear_bmpheader(header);
}


static enum read_status read_bmpheader(FILE *input_file, struct bmp_header *header) {
    if (fread(header, BMP_HEADER_SIZE, 1, input_file) != 1 || header->bfType != BMP_TYPE) {
        return READ_INVALID_HEADER;
    }
    return READ_OK;

}


enum read_status from_bmp(FILE *input_file, struct image *image) {
    struct bmp_header bmp_header;
    if (read_bmpheader(input_file, &bmp_header) != READ_OK) {
        return READ_INVALID_HEADER;
    }
    image->width = bmp_header.biWidth;
    image->height = bmp_header.biHeight;
    size_t row_padding = GET_ROW_PADDING(image);
   size_t data_size =  GET_IMAGE_SIZE(image);
   image->data = (struct pixel*) malloc(data_size);
    if(!image->data){
        return READ_FAILURE;
    }
    for (size_t i = 0; i < image->height; i++) {
    for (size_t j = 0; j < image->width; j++) {
        if(fread(&image->data[i * image->width + j], sizeof(struct pixel), 1, input_file)!=1){
            free(image->data);
            return READ_FAILURE;
        }
    }
        if (fseek(input_file, (long)row_padding, SEEK_CUR)!=0) {
            free(image->data);
            return READ_FAILURE;
        }
}
    return READ_OK;
}


enum write_status to_bmp(FILE *output_file, struct image const *image) {
    size_t padding = GET_ROW_PADDING(image);
    write_bmpheader(output_file, image);
    for (size_t i = 0; i < image->height; i++) {
        for (size_t j = 0; j < image->width; j++) {
            if (fwrite(&(image->data[i * image->width + j]), sizeof(struct pixel), 1, output_file) == 0) {
                return WRITE_FAILURE;
            }
        }
        if (padding) {
            for (size_t k = 0; k < padding; k++) {
                if(fputc(PADDING_BYTE, output_file)){
                    return WRITE_FAILURE;
                }
            }
        }
    }
    free(image->data);
        return WRITE_OK;
    }



