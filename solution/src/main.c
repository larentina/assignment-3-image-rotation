#include "bmp_file.h"

#include "image.h"

#include "fileio.h"
#include <stdio.h>
#include <stdlib.h>

#define ANGLE_INDEX 3
#define REQUIRED_ARGS_COUNT 4
#define IFILE_INDEX 1
#define OFILE_INDEX 2
int main(int argc, char **argv) {

    if (argc != REQUIRED_ARGS_COUNT) {
        fprintf(stderr,"Invalid input format");
    }
    int32_t angle = atoi(argv[ANGLE_INDEX]);

    if (abs(angle)!= 270 && abs(angle)!= 180 && abs(angle)!= 90 && angle!= 0 ) {
        fprintf(stderr,"Invalid angle");
        return EXIT_FAILURE;
    }

    struct image* image = malloc(sizeof(struct image));
    FILE* input_file;
    if(open_file(argv[IFILE_INDEX], &input_file, "rb") != IO_SUCCESS){
        clear_image(image);
        fprintf(stderr,"Could not open file");
        return EXIT_FAILURE;
    }

enum read_status result = from_bmp(input_file, image);
    if(result != READ_OK){
        clear_image(image);
        if(result == READ_INVALID_HEADER){
            fprintf(stderr,"Invalid header in input file");
        }
        if(result == READ_FAILURE){
            fprintf(stderr,"Could not read bmp-file");
        }
        return EXIT_FAILURE;
        }

    if(close_file(input_file)!= IO_SUCCESS){
        clear_image(image);
        fprintf(stderr,"Could not close file");
        return EXIT_FAILURE;
    }

    struct image* new_image = rotate_to_angle(*image,angle);
    clear_image(image);
    if(!new_image){
        fprintf(stderr, "Could not rotate image");
        return EXIT_FAILURE;
    }

    FILE *output_file;
    if(open_file(argv[OFILE_INDEX], &output_file, "wb") != IO_SUCCESS){
        clear_image(new_image);
        fprintf(stderr, "Could not open file");
        return EXIT_FAILURE;
    }

enum write_status status = to_bmp(output_file, new_image);
   clear_image(new_image);
    if(status != WRITE_OK){
        if(status == WRITE_FAILURE){
            fprintf(stderr, "Could not write in output bmp-file");
        }
        return EXIT_FAILURE;
    }
    if(close_file(output_file)!= IO_SUCCESS){
        fprintf(stderr, "Could not close file");
        return EXIT_FAILURE;
    }
    return 0;

}

