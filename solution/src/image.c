
#include "image.h"
#include <malloc.h>


struct image* rotate_to_angle(struct image source,int32_t angle){
    int32_t number_rotations = ((360+angle)%360 )/ 90;
    struct image* new_image= malloc(sizeof (struct image));
    if(!new_image){
        return NULL;
    }
    *new_image =source;
    for(size_t i=0;i<number_rotations;i++){
        struct image temp = rotate(*new_image);
        free(new_image->data);
        if(!temp.data){
            clear_image(new_image);
            return NULL;
        }

        *new_image = temp;

    }
    return new_image;

}
void clear_image(struct image* image ){
    free(image);
}

struct image rotate(struct image const source){
    struct image new_image={
            .width = source.height,
            .height = source.width,
            .data =  (struct pixel*) malloc(sizeof (struct pixel) * source.height*source.width)
    };
    if(!new_image.data){
        return new_image;
    }

    for (size_t i = 0; i < new_image.height; i++) {
        for (size_t j = 0; j < new_image.width; j++) {
            new_image.data[(new_image.height - i - 1) * new_image.width + j] = source.data[j * source.width + i];
        }
    }
 return new_image;
}
